## Info data schemes
```
    payment_models = {
        "CafeBazar": {
            "OrderId": None,
            "Signature": None,
            "Sku": None,
            "PurchaseTIme": None,
            "Message": None,
            "Response": None,
            "Price": None
        },
        "BankGateAway": {
            "TerminalNumber": None,
            "ReferenceNumber": None,
            'CardNumber': None,
            "TransactionTime": None,
            "TransactionStatus": None,
            "ReceiverNumber": None,
            "Amount": None

        },
        "BuyAnimation": {
            "AnimationId": None,
            "SeasonNumber": None
        },
        "ViewCharge":{
            "AnimationId":None,
            "Amount":None,
            "TotalSecond":None
        }
    }
```
bbin baste be key _from k entekhab mikoni bayad kilid info bar ba format khasi bedi

## Charge
```
URL /users/accounting/balance/charge
METHOD POST
PORT 3005
HEADER Token Phonenumber
BODY Amount,From,Info
From = ["CafeBazar","BankGateAway","BuyAnimation","ViewCharg"]
```
be onvan mesal agar From o CafeBazar entekhab koni bayad Info o ba in kilid ha bedi 
```
        {
            "OrderId": None,
            "Signature": None,
            "Sku": None,
            "PurchaseTIme": None,
            "Message": None,
            "Response": None,
            "Price": None
        }
```

deghat kon k in kilid ha bayad be string e json tabdil beshan va ba kilid Info be body dade beshan
```
"Info":json.dumps({
            "OrderId": "Amin",
            "Signature": "Amin",
            "Sku": "Amin",
            "PurchaseTIme": "Amin",
            "Message": "Amin",
            "Response": "Amin",
            "Price": 12000
        }
```

deghat kon k tu bara charge tu halate ViewCost bayad Amount '-' manfi bashe

body ha ham json hastan

# python code sample
```
def charge():
    a = requests.post("http://localhost:3005/users/accounting/balance/charge",
                      json={
                          "Amount":12000,
                          "From":"CafeBazar",
                          "Info":json.dumps({
            "OrderId": "Amin",
            "Signature": "Amin",
            "Sku": "Amin",
            "PurchaseTIme": "Amin",
            "Message": "Amin",
            "Response": "Amin",
            "Price": 12000
        })
                      },headers=headers).content
    print(a)

```

# sample ViewCharge
```
def charge_viewcost():
    a = requests.post("http://localhost:3005/users/accounting/balance/charge"
    , headers = {
        "Phonenumber": "09119518867",
        "Token": "0fb09130698f0c49dd46ff93778f1d495f2fb3882f857061b32ad018ca79e9e2"
    },
                      json={
                          "Amount": -12300,
                          "From": "ViewCharge",
                          "Info": json.dumps({
                              "AnimationId": "Amin",
                              "TotalSecond": 230,
                          })
                      }).content
    print(a)
```
