## Sync-Balance-view-cost
```
URL /users/balance
Method GET
Header Token Phonenumber
PORT 3002
```



# auhtentication

 PORT 3002
## signup
### in ghesmate baraie ezafe kardn esm o famil o hozuri ba ham sohbat mikonim
```
URL users/signup/<phonenumber>
PORT 3002
HEADER Content-type = application/json
BODY FirstName , LastName
Method POST
```
## code verification
```
URL /users/verifyCode/<phonenumber>/<code>
PORT 3002
HEADER None
BODY None
Method GET
```
## login
```
URL /users/login/<phonenumber>
Method GET
Header None
Body None
```
## logout
```
URL /users/logout
Method GET
Body None
Header Token Id
```

# user property
## get user profile
```
URL /users/ProfileImage
Method GET
Header Id Token
Body None
```
## get user profile by phonenumber
```
URL /users/ProfileImage/<phonenumber>
Method GET
Header Phonenumber , Token
Body None
```
## update user profile
### in ghesmato hozuri ba ham ok mikonim
```
URL /users/profile/update
Method POST
Header Content-Type = application/json
Body Fname Lname Email Birthday Language
```

## Get User Info
```
URL /users/getinfo
Method GET
Header Token Phonenumber
Body None
```
## User Get Admin Message
```
URL /users/admingroupmessage/get
Method GET
Header Token Phonenumber
Body None
```
## User Seen Admin Message
```
URL /users/adminmessage/seen
Method POST
Header Token Phonenumber
Body MessageId
```
## Get Group Admin Message
```
URL /users/adminmessage
Method GET
Header Token Phonenumber
Body
```

## GET New Admin Message
```
URL /users/adminmessage/newmessage/get
Method GET
Header Token Phonenumber
Body
```
## User send admin message
```
URL /users/adminmessage/send
Method POST
Header Content-Type = application/json Token Phonenumber
Body Page Title Description
```
## search user
```
URL /users/search
Method POST
Header Token Phonenumber
Body Value
```
# Bookmarks

## Add Book Mark
```
URL /users/bookmark/add
Method POST
Header Token Phonenumber
Body AnimationId
```
## delete bookmark
```
URL /users/bookmark/delete/<animId>
Method GET
Header Token Phonenumber
Body None
```
## get user bookmarks
```
URL /users/bookmark/
Method Get
Header Token Phonenumber
Body None
```

# Profile
## Update Profile
baraie upload profile inja tasvir o be base64 tabdil mikonam man ba ye function e 
khode function o mizaram barat bbin moadelesh chie

## base64 function
```
def filetobase64(imagepath):
    with open(imagepath, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
        base64_bytes = base64.b64encode(encoded_string)

        # third: decode these bytes to text
        # result: string (in utf-8)
        base64_string = base64_bytes.decode('utf-8')
        print(base64_string)

        # optional: doing stuff with the data
        # result here: some dict

        return base64_string
```
```
URL  /users/profile/update
Method POST
Header Token Phonenumber
Body {"Fname": 'amin',
     'Lname': 'jamal',
     'Birthday': '1,1,1996',                                                                                                        'Email': 'aminjamal10@gmail.com',
     "Profile": filetobase64(),
     "Language": "mazani"}
PORT 3002
```
## Get Profile Image
```
URL /users/ProfileImage
PORT 3002
Header Token Phonenumber
Body  None
```

# user profile

## update information
```
URL /users/profile/information/update
Method POST
Header Token Phonenumber
Body Fname Lname Birthday Email Language
```

## upload image
```
URL /users/profile/image/update
Method POST
Header Token Phonenumber
Body File FileName . [ File "base64 shodeie file" , FileName esme file]
```

## get info
```
URL /users/getinfo
Method GET
Header Token Phonenumber
Body None
```

## User Service
```
URL /users/profile/information/update/language
Method POST
Header Token Phonenumber
Body NewLanguages
```
