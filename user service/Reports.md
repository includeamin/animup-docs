# GET ADMIN REPORS
```
URL /admin/userreport
METHOD GET
HEADER Token Phonenumber
BODY None
PORT 3002
```
# GET ADMIN REPORTS STATUS
```
URL /admin/userreport/<status>
METHOD GET
HEADER Token Phonenumber
BODY None
PORT 3002
```
# CHANGE ADMIN REPORTS STATUS
```
URL /admin/userreport/check/<id>/<status>
METHOD GET
BODY NONE
HEADER Token Phonenumber
PORT 3002
```
