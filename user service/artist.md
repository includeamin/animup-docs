# Admin Section
## add Artist
```
URL /admin/artist/add
Header Token Phonenumber
Method POST
Body form["Fname"], form["Lname"], form["Birthday"], form["Bio"], form["Expertise"]
```
 ## delete artist by id
 
 ```
 URL /admin/artist/delete/<id>
 Method GET
 Header Token Id
 Body None
 ```


# Mobile Section

## get artist image by id
```
URL /artist/image/get/<id>
Method GET
Header Token Phonenumber
Body None
```
## get artist list
```
URL /artist
Method GET
Header Token Phonenumber
Body None
```
## Update
```
URL /admin/artist/{_id}/update
Method POST
Header Token Phonenumber
Body NewData [kole data e hast k az server migirin ]
```
