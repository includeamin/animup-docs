import requests

base_url = "http://89.32.251.131:3002"

header = {'Phonenumber':"09362810322","Token":"2c0f5662c5dbc2e49e8a070edebe7ae8c34b1b24c7408184c4ee54ad7cd6eca2"}

def write_log(name,log):
    with open(f"{name}.txt",'w') as f:
        f.write(str(log))
        f.close()


def signup(phonenumber):
    a = requests.post(f"{base_url}/users/signup/{phonenumber}").content
    print(a)
    write_log("SignUp",a)


def active_account(phonenumber, code):
    a = requests.get(f"{base_url}/users/verifyCode/{phonenumber}/{code}").content
    print(a)
    write_log("ActiveAccount",a)




if __name__ == '__main__':
    active_account("09362810322","9118")
