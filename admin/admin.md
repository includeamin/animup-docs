# Admin

## Publisher

## GET Publishers
```
URL /admin/publishers/get/<status>
status = ["INPROGRESS", "CONFIRM", "NOTCONFIRM", "BAN"] none case sensetive
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## Accept Publishers
```
URL /admin/publishers/status/change/<newstatus>/<id>
newstatus = ["CONFIRM", "NOTCONFIRM", "BAN"] none case sensetive
Method POST
Header Token Phonenumber
Body [for Ban :[Message,Day]]
PORT 3002
```
## GET PUBLISHER
```
URL /admin/publisher/info/<phonenumber>
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET PULISHERS UPLOADS (SNN,AGAHI,RUZNAME_RASMI,PROFILE)
```
URL /admin/publisers/uploads/get/<image>/<phonenumber>
image = [agahi,rusnamerasmi,ssn,profile]
Method GET
Header Token Phonenumber
Body None
Response is link 
PORT 3002
```
# Animations
## add/clean animation review
```
URL /admin/animation/review/<action>/<animationid>
action = [add/remove]
Method POST
Header Token Phonenumber
Body form["Content"],form["Title"]
PORT 3003
```
## GET ALL ANIMATION REVIEWS
```
URL /admin/animation/reviews
Method GET
Header Token Phonenumber
Body None
Port 3003
```
## Delete animation by id
```
URL /admin/animation/delete/<id>
id = animation id
Method GET
Header Token Phonenumber
Body None
Port 3003
```

## Get Animation Information
```
URL
Method
Header
Body
```
## Change Animation Status
```
URL /admin/animations/status/change/<animationid>/<newstatus>
status = ["Ban".upper(), "confirm".upper(), "notconfirm".upper()]
Method POST
Header Token Phonenumber
Body  form["Message"]
PORT 3002
send admin message to publisher
```

## GET Animation Status (inprogress)
```
URL /admin/animations/inprogress
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET Animation Status (confirm)
```
URL /admin/animations/confirm
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET Animation Status (ban)
```
URL /admin/animations/ban
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET Animation Status (notconfirm)
```
URL /admin/animations/notconfirm
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET Animation Status (requested)
```
URL /admin/animations/requested
Method GET
Header Token Phonenumber
Body None
PORT 3002
```

# Users

## Get Users Info
```
URL /admin/users/get/<id>
id = user's id
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET USER with inprogress status
```
URL /admin/users/get/inprogress
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET USER WITH ACCEPTED STATUS
```
URL /admin/users/get/accepted
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
## GET USER WITH BAN STATUS
```
URL /admin/users/get/baned
Method GET
Header Token Phonenumber
Body None
PORT 3002
```

## Change User Status
```
URL /admin/users/status/<id>/<status>
sts = ["CONFIRM", "NOTCONFIRM", "BAN"] none sensetive
id = user's id
Method POST
Header Token Phonenumber
Body for Ban [Day,Message]
PORT 3002
```
## SET USER DEFAULT DATA (todo)
```
URL /admin/users/setdefault
Method
Header
Body 
PORT 3002
```
# Banners 

## Add
```
URL /admin/banners/upload/<animationid>
Method POST
Header Token Phonenumber
Body AnimationName , Banner[File],FileName
PORT 3004
```
## GET BANNER MOBILE
```
URL /mobile/banners/download/<id>
Method GET
Header Token Phonenumber
Body None
PORT 3004
```
## GET ALL BANNERs
```
URL /mobile/banners
Method GET
Header Token Phonenumber
Body None
PORT 3004
```

## Delete
```
URL /admin/banners/delete/<id>
Method GET
Header Token Phonenumber
Body None
```
## add click count banner
```
URL /mobile/banner/click/<id>
Method GET
Header Token Phonenumber
Body None
```
# Artists

## Add
```
URL /admin/artist/add
Method POST
Header Token Phonenumber
Body form["Fname"], form["Lname"], form["Birthday"], form["Bio"], form["Expertise"], form["ImageId"]
PORT 3002
```
## GET
```
URL /artist
Method GET
Header Token Phonenumber
Port 3002
```

## Delete
```
URL /admin/artist/delete/<id>
Method GET
Header Token Phonenumber
Body None
```
# Languages

## Add/Delete
```
URL /language/[add or delete]
Method POST
Header Token Phonenumber
Body LanguageName
PORT 3003
```
## Get
```
URL /language
Method GET
Header Token Phonenumber
Body None
PORT 3003
```
## Age Range Add/Delete
```
URL /age/[add or delete]
Method POST
Header Token Phonenumber
Body AgeName
PORT 3003
```
## GET AGE
```
URL /age
Method GET
Header Token Phonenumber
Body None
Port 3003
```

## Genre Add/Delete
```
URL /genre/[add or delete]
Method POST
Header Token Phonenumber
Body GenreName
Port 3003
```
## Genre GET
```
URL /genre
PORT 3003
Method GET
Body None
Header Token Phonenumber
```

# Messages
## Seach user based on phonenumber or fname or lname
```
URL /admin/users/get/list-for-admin-message
Method POST
Header Token Phonenumber
Body Value
PORT 3003
```
## SEND GROUP MESSAGE
```
URL /admin/adminmessage/send/group
groups = ["user", "publisher"]
Method POST
Header Token Phonenumber
Body   form["GroupName"],Message,Title
PORT . 3002
```
## Send Message to Users
```
URL /admin/users/adminmessage
Method POST 
Header Token Phonenumber
Body  message = form["Message"]   user_phonenumber = form["PhoneNumber"]
PORT 3002
```
## Send Message to Publisher (bala)
```
URL
Method
Header
Body
```

## Delete Messages
```
URL /users/adminmessagegroup/delete/<id>
Method GET
Header Token Phonenumber
Body None
PORT 3002
```
# Category

## add

```
URL  /category/add
Method POST
Header Token Phonenumber
Body CategoryName ImageId
PORT 3003
```
## delete
```
URL /category/delete
Method POST
Header Token Phonenumber
PORT 3003
Body CategoryName
```
## Get
```
URL /category
Method GET
Header Token Phonenumber
Body None
Port 3003
```

## Submit Off ( Takhfif )
```
URL
Method
Header
Body
```
# Tag
## add
```
URL /tag/[add or delete]
Method POST
Header Token Phonenumber
Port 3003
Body  TagName
```

## get
```
URL /tag
Method GET
Header Phonenumber Token
PORT 3003
Body None
```

# ConfirmType
## Add/Delete
```
URL /confirmtype/[add or delete]
Method POST
Header Token Phonenumber
Body TypeName
PORT 3003
```
## GET
```
URL /confirmtype
Method GET
Header Token Phonenumber
Body None
Port 3003
```
