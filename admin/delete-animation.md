## DELETE
```
URL /admin/animation/delete/<animationid>
Method GET
Header Token Phonenumber
Body None
```
## FINAL DELETE
```
URL /admin/animation/delete/final/<id>
Method GET
Header Token Phonenumber
Body None
```
## UNDO DELETE
```
URL /admin/animation/deleted/undo/<id>
Method GET
Header Token Phonenumber
Body None
```
## GET DELTED
```
URL /admin/animation/deleted
Method GET
Header Token Phonenumber
Body None
```
