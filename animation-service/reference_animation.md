##
```
    valid_animation_types = {"serial": "serial", "serial-sinamae": "serial-sinamae", "sinamae": "sinamae"}

```

## ADD
```
URL /admin/animation/reference/add
Method POST
Port 3003
Header Token Phonenumber
Body
form["Anim_Name"], 
anim_poster=form["Anim_Poster"],
 anim_cover=form["Anim_Cover"],
 director=form["Director"],
 publishername=form["PublisherName"],
                                  date=form["Date"],
                                  producer=form["Producer"],
                                  genre=form["Genre"],
                                  age=form["Age"],
                                  producer_group=form["ProducerGroup"],
                                  taglist=form["TagList"],
                                  description=form["Description"],
                                  category=form["Category"],
                                  is_active=form["IsActive"],
                                  is_series=form["IsSeries"],
                                  animation_type=form["AnimationType"]
```
## GET ALL
```
URL /admin/animation/reference/all
Method GET
Port 3003
Header Token Phonenumber
Body None
```
## GE by Name
```
URL /admin/animation/reference/get-by-name
Method POST
Port 3003
Header Token Phonenumber
Body Name
```
## Remove sub animation from reference aniamtion
```
URL /admin/animation/reference/<name>/delete/sub-animation/<animation_id>/<anim_lang>
Method GET
Port 3003
Header Token Phonenumber
Body None
```
## Delete
```
URL /admin/animation/reference/<id>/delete
Method GET
Port 3003
Header Token Phonenumber
Body None
```
# Search
```
URL /admin/animation/reference/search/by-name
Method POST
Port 3003
Header Token Phonenumber
Body POST
```

# Update Reference animation activation directly
```
URL /admin/animation/reference/<id>/activation/<is_active>
Method GET
Port 3003
Header Token Phonenumber
Body None
```
